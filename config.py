import os
from os import getenv

from dotenv import load_dotenv

if os.path.exists("local.env"):
    load_dotenv("local.env")

que = {}
SESSION_NAME = getenv("SESSION_NAME", "session")
BOT_TOKEN = getenv("BOT_TOKEN")
BOT_NAME = getenv("BOT_NAME")
UPDATES_CHANNEL = getenv("UPDATES_CHANNEL", "sadnesstalk")
BG_IMAGE = getenv("BG_IMAGE", "https://telegra.ph/file/9c365d52e88bb727bfb24.png")
PLAY_IMG = getenv("PLAY_IMG", "https://telegra.ph/file/9c365d52e88bb727bfb24.png")

admins = {}
API_ID = int(getenv("API_ID"))
API_HASH = getenv("API_HASH")
BOT_USERNAME = getenv("BOT_USERNAME")
ASSISTANT_NAME = getenv("ASSISTANT_NAME", "antigabutbrothers")
SUPPORT_GROUP = getenv("SUPPORT_GROUP", "antigabutbrothers")
PROJECT_NAME = getenv("PROJECT_NAME", "sadnesstalk")
SOURCE_CODE = getenv("SOURCE_CODE", "https://bitbucket.org/farah21/anushkamusic")
DURATION_LIMIT = int(getenv("DURATION_LIMIT", "7"))
ARQ_API_KEY = getenv("ARQ_API_KEY", None)
PMPERMIT = getenv("PMPERMIT", None)
LOG_GRP = getenv("LOG_GRP", None)
DATABASE_URL = os.environ.get("DATABASE_URL", "mongodb+srv://userbot:userbot@cluster0.quwam.mongodb.net/userbot?retryWrites=true&w=majority")

COMMAND_PREFIXES = list(getenv("COMMAND_PREFIXES", "/ !").split())

SUDO_USERS = list(map(int, getenv("SUDO_USERS").split()))
